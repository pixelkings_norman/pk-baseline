'use strict';

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        shell: {
            bundler: {
                command: 'bundle'
            },
            bower: {
                command: 'bower install --config.interactive=false'
            }
        },

        bower_concat: {
            all: {
                dest: 'js/_bower.js',
                exclude: [
                    'Cortana'
                ]
            }
        },

        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'css/app-unprefixed.css':'sass/app.scss'
                }
            }
        },

        postcss: {
            options: {
                processors: [
                    require('autoprefixer')({browsers: ['last 3 version']})
                ]
            },
            dist: {
                src: 'css/app-unprefixed.css',
                dest: 'css/app.css'
            }
        },

        hologram: {
            generate: {
                options: {
                    config: './hologram/hologram_config.yml'
                }
            }
        },

        connect: {
            server: {
                options: {
                    port: 9001,
                    base: 'design',
                    open: {
                        target: 'http://localhost:9001/styleguide/'
                    }
                }
            }
        },

        watch: {
            sass: {
                files: ['sass/**/*'],
                tasks: ['sass:dist','postcss:dist','copy'],
                options: {
                    livereload: true
                }
            },
            hologram: {
                files: ['sass/**/*'],
                tasks: ['hologram']
            },
            livereload: {
                options: {
                    livereload: 1337
                },
                files: [
                    'design/styleguide/*.html'
                ]
            },
            copy: {
                files:['sass/**/*'],
                task: ['copy:main']
            },
            grunticon: {
                files:['img/icons/raw/**/*'],
                task: ['grunticon:myIcons']
            }
        },

        clean: {
            default: {
                src: ['design/styleguide']
            },
            remove_info_files: {
                src: ['node_modules/**/*.info']
            }
        },

        //minimize SVG files

        svgmin: {
            options: {
                plugins: [
                    { removeViewBox: false },
                    { removeUselessStrokeAndFill: false }
                ]
            },
            dist: {
                expand: true,
                cwd: 'img/icons/raw',
                src: ['*.svg'],
                dest: 'img/icons/min'
            }
        },

        grunticon: {
            myIcons: {
                files: [{
                    expand: true,
                    cwd: 'img/icons/min',
                    src: ['*.svg', '*.png'],
                    dest: "img/icons/dist"
                }],
                options: {
                    cssprefix: ".",
                    loadersnippet: "grunticon.loader.js",
                    defaultWidth: "96px",
                    defaultHeight: "96px"
                }
            }
        },

        csscss: {
            dist: {
                src: ['design/css/app.css']
            },
            options: {
                colorize: false,
                verbose: true,
                outputJson: true,
                minMatch: 10,
                compass: true
            }
        },

        copy: {
            main: {
                files: [
                    { expand: true, src: ['css/**','js/**','img/**','fonts/**'], dest: 'design/' }
                ]
            }

        }

    });

    grunt.registerTask('icons', [
        'svgmin',
        'grunticon',
        'copy'
    ]);

    grunt.registerTask('default', [
        'sass',
        'postcss:dist',
        'connect',
        'csscss',
        'copy:main',
        'hologram',
        'watch'
    ]);

    grunt.registerTask('compile', [
        'clean:default',
        'sass',
        'postcss:dist',
        'hologram',
        'copy:main'
    ]);

    grunt.registerTask('install', [
        'shell:bundler',
        'shell:bower',
        'sass',
        'postcss:dist',
        'connect',
        'csscss',
        'copy:main',
        'hologram',
        'svgmin',
        'grunticon',
        'clean:remove_info_files'
    ]);
};
