/*
 * initiate isotope
 */

jQuery( document ).ready(function( $ ) {

    $(".social").on({
        "touchstart": function(){
            $(this).addClass('social-hover',500);
        },
        "touchend": function(){
            $(this).removeClass('social-hover',500);
        }
    });


    $('.social__item--more').on("click", function (e) {
        $(this).closest(".social--horizontal").addClass('social--horizontal--showall');
        e.preventDefault();

    });


    $('.js-show-more').on("click", function (e) {

        var $this = $(this);

        //add class to show all
        $this.toggleClass('btn--less').closest('.reveal').toggleClass('reveal--showall');

        //create array of data-attribute
        var labelArray =  $this.attr('data-toggle-label').split('|');

        //toggle label
        if ($this.text() == labelArray[0])  {
            $this.text(labelArray[1]);
        } else {
            $this.text(labelArray[0]);
        }

        e.preventDefault();

    });


    $('.mainnav__l1 li').on("click", function (e) {

        // add class to open subitems
        $(this).toggleClass("open");
    });


    $(".mainnav__l2 li").on({
        mouseenter: function(){
            $('.menu--flyout').addClass('show-flyout');
        }
        // voor nu laten we de flyout staan op mouseleave
        //,mouseleave: function(){
        //    $('.menu--flyout').removeClass('show-flyout');
        //}
    });

    // scroll to top
    $(".js-scrolltop").on("click", function(e) {
        $('html, body').animate({ scrollTop: 500 }, 0).animate({ scrollTop: 0 }, 1000);
        e.preventDefault();
    });

});


$(document).scroll(function(){

    var windowsize = $(window).width();

    //console.log(windowsize);

    if ( windowsize > 665 ) {

        $('.mainnav__l2').addClass('hide');
        $('.menu--flyout').removeClass('show-flyout');

        var scrollA = $('body').scrollTop();

        setTimeout(function () {

            if (scrollA == $('body').scrollTop()) {
                $('.mainnav__l2').removeClass("hide");
            }

        }, 200);

    }

});






//$('.social__item--more').on("touchstart", function (e) {
//    console.log("touch");
//    'use strict'; //satisfy code inspectors
//    var link = $(this); //preselect the link
//    if (link.hasClass('social-hover')) {
//        return true;
//    } else {
//        link.addClass('social-hover');
//        $('social-hover').not(this).removeClass('hover');
//        e.preventDefault();
//        return false; //extra, and to make sure the function has consistent return points
//    }
//});

